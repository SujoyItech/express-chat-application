function decorateHtmlResponse(route_title) {
  return function (req, res, next) {
    res.locals.html = true;
    res.locals.title = `${route_title} - ${process.env.APP_NAME}`;
    next();
  };
}

module.exports = decorateHtmlResponse;
